# DKX/iConsole

Wrapper of iConsole treadmill bluetooth protocol.

## Installation

```bash
$ npm install --save @dkx/iconsole
```

## Example

* [Watch sensors](./example/index.ts)
