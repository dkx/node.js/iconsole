import {DefaultDeviceConfiguration, IConsole} from '../src';


async function main(): Promise<void>
{
	if (typeof process.env.ICONSOLE_ADDRESS === 'undefined') {
		throw new Error('Please provide iConsole device address with ICONSOLE_ADDRESS environment variable');
	}

	const iConsole = new IConsole(
		new DefaultDeviceConfiguration(),
		{
			discoverMaxDevices: 20,
			deviceRetryTimeout: 500,
			deviceWatchSensorsInterval: 200,
			deviceResponseTimeout: 2000,
		},
	);

	process.stdout.write('Searching...');
	const device = await iConsole.findDevice(process.env.ICONSOLE_ADDRESS);
	console.log(' OK');

	if (device === null) {
		throw new Error('No iConsole compatible device found');
	}

	process.stdout.write('Connecting...');
	await device.connect();
	console.log(' OK');

	console.log('Watching sensors...');
	await device.watchSensorsChanges((data) => {
		console.log(`unknown: ${data.unknownData} | elapsed: ${data.elapsed}s | speed: ${data.speed}m/s | distance: ${data.distance}m | incline: ${data.incline}% | calories: ${data.calories} | pulse: ${data.pulse}`);
	});

	process.stdout.write('Disconnecting...');
	await device.disconnect();
	console.log(' OK');

	process.exit(0);
}

main()
	.then(() => process.exit(0))
	.catch(err => {
		console.error(err);
		process.exit(1);
	});
