import {EventEmitter} from 'events';
import {Characteristic, Peripheral, Service} from '@abandonware/noble';
import {waitPromise} from '@dkx/wait-promise';
import {retryPromise} from '@dkx/retry-promise';
import Timeout = NodeJS.Timeout;

import {SensorsData} from './sensors-data';
import {DeviceConfigurationInterface} from './device-configurations';


const DEFAULT_CONNECTION_TIMEOUT = 5000;
const DEFAULT_RETRY_TIMEOUT = 500;
const DEFAULT_WATCH_SENSORS_INTERVAL = 300;
const DEFAULT_RESPONSE_TIMEOUT = 1000;

export declare interface DeviceOptions
{
	connectionTimeout?: number,
	connectionRetries?: number,
	retryTimeout?: number,
	watchSensorsInterval?: number,
	responseTimeout?: number,
}

export class Device extends EventEmitter
{
	private serviceConsole: Service|null = null;

	// write, write_no_response
	private _characteristicConsoleWrite: Characteristic|null = null;

	// notify, write, write_no_response
	private _characteristicConsoleNotify: Characteristic|null = null;

	private readonly connectionTimeout: number;

	private readonly connectionRetries: number;

	private readonly retryTimeout: number;

	private readonly watchSensorsInterval: number;

	private readonly responseTimeout: number;

	constructor(
		private readonly configuration: DeviceConfigurationInterface,
		private readonly peripheral: Peripheral,
		options: DeviceOptions = {},
	) {
		super();

		this.connectionTimeout = options.connectionTimeout ?? DEFAULT_CONNECTION_TIMEOUT;
		this.connectionRetries = options.connectionRetries ?? 1;
		this.retryTimeout = options.retryTimeout ?? DEFAULT_RETRY_TIMEOUT;
		this.watchSensorsInterval = options.watchSensorsInterval ?? DEFAULT_WATCH_SENSORS_INTERVAL;
		this.responseTimeout = options.responseTimeout ?? DEFAULT_RESPONSE_TIMEOUT;
	}

	private get characteristicConsoleWrite(): Characteristic
	{
		if (this._characteristicConsoleWrite === null) {
			throw new Error('Device is not connected');
		}

		return this._characteristicConsoleWrite;
	}

	private get characteristicConsoleNotify(): Characteristic
	{
		if (this._characteristicConsoleNotify === null) {
			throw new Error('Device is not connected');
		}

		return this._characteristicConsoleNotify;
	}

	public async connect(): Promise<void>
	{
		const connect = (): Promise<void> => {
			const timeout = new Promise<void>((resolve, reject) => {
				const id = setTimeout(() => {
					clearTimeout(id);
					reject(new Error('Connection timeout'));
				}, this.connectionTimeout);
			});

			return Promise.race([
				this.peripheral.connectAsync(),
				timeout,
			]);
		};

		await retryPromise(() => connect(), {
			retries: this.connectionRetries,
		});

		this.peripheral.once('disconnect', async err => {
			this.serviceConsole?.removeAllListeners();
			this._characteristicConsoleWrite?.removeAllListeners();
			this._characteristicConsoleNotify?.removeAllListeners();

			this.serviceConsole = null;
			this._characteristicConsoleWrite = null;
			this._characteristicConsoleNotify = null;

			this.emit('disconnect', err);
		});

		const services = await this.peripheral.discoverServicesAsync([this.configuration.serviceConsoleUuid]);
		const serviceConsole = services.find(s => s.uuid === this.configuration.serviceConsoleUuid);

		if (typeof serviceConsole === 'undefined') {
			throw new Error(`Device: service ${this.configuration.serviceConsoleUuid} was not found`);
		}

		this.serviceConsole = serviceConsole;

		const consoleCharacteristics = await this.serviceConsole.discoverCharacteristicsAsync([
			this.configuration.consoleWriteCharacteristicUuid,
			this.configuration.consoleNotifyCharacteristicUuid,
		]);

		const characteristicConsoleWrite = consoleCharacteristics.find(ch => ch.uuid === this.configuration.consoleWriteCharacteristicUuid);
		if (typeof characteristicConsoleWrite === 'undefined') {
			throw new Error(`Device: characteristic ${this.configuration.consoleWriteCharacteristicUuid} was not found in service ${this.configuration.serviceConsoleUuid}`);
		}

		const characteristicConsoleNotify = consoleCharacteristics.find(ch => ch.uuid === this.configuration.consoleNotifyCharacteristicUuid);
		if (typeof characteristicConsoleNotify === 'undefined') {
			throw new Error(`Device: characteristic ${this.configuration.consoleNotifyCharacteristicUuid} was not found in service ${this.configuration.serviceConsoleUuid}`);
		}

		this._characteristicConsoleWrite = characteristicConsoleWrite;
		this._characteristicConsoleNotify = characteristicConsoleNotify;

		await this.characteristicConsoleNotify.notifyAsync(true);
		await this.ping();
	}

	public async disconnect(): Promise<void>
	{
		await this.peripheral.disconnectAsync();
	}

	public ping(): Promise<void>
	{
		return this.sendDataAndExpectResponse(this.configuration.pingRequestData, this.configuration.pingResponseData);
	}

	public async watchSensors(watcher: (data: SensorsData) => void): Promise<void>
	{
		const checkStatus = async () => {
			let data: Buffer;

			try {
				data = await this.sendDataAndWaitForResponse(this.configuration.sensorsStateRequestData);
			} catch (e) {
				return;
			}

			watcher(this.configuration.parseSensorsData(data));
			await waitPromise(this.watchSensorsInterval);
			await checkStatus();
		};

		await checkStatus();
	}

	public async watchSensorsChanges(watcher: (data: SensorsData) => void): Promise<void>
	{
		let previous = new SensorsData(Buffer.from([]), '', 0, 0, 0, 0, 0, 0);

		await this.watchSensors(data => {
			if (!previous.equals(data)) {
				previous = data;
				watcher(data);
			}
		});
	}

	private sendData(data: Buffer): Promise<void>
	{
		return this.characteristicConsoleWrite.writeAsync(data, false);
	}

	private async sendDataAndWaitForResponse(data: Buffer): Promise<Buffer>
	{
		await this.sendData(data);

		return await retryPromise<Buffer>(async () => {
			return new Promise((resolve, reject) => {
				let timeout: Timeout|null = null;
				let handler: ((data: Buffer) => void)|null = null;

				timeout = setTimeout(() => {
					if (handler !== null) {
						this.characteristicConsoleNotify.removeListener('read', handler);
					}

					reject(new Error('Response timeout'));
				}, this.responseTimeout);

				handler = (data: Buffer): void => {
					if (timeout !== null) {
						clearTimeout(timeout);
					}

					resolve(data);
				};

				this.characteristicConsoleNotify.once('read', handler);
			});
		}, {
			delay: this.retryTimeout,
		});
	}

	private async sendDataAndExpectResponse(send: Buffer, expectedResponse: Buffer): Promise<void>
	{
		const response = await this.sendDataAndWaitForResponse(send);
		if (!response.equals(expectedResponse)) {
			throw new Error(`Expected ${expectedResponse.toString('hex')}, but received ${response.toString('hex')}`);
		}
	}

	private reset(): void
	{
		this.serviceConsole?.removeAllListeners();
		this._characteristicConsoleWrite?.removeAllListeners();
		this._characteristicConsoleNotify?.removeAllListeners();

		this.serviceConsole = null;
		this._characteristicConsoleWrite = null;
		this._characteristicConsoleNotify = null;
	}
}
