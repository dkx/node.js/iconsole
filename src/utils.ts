export function kilometersPerHourToMetersPerSecond(speed: number): number
{
	return speed / 3.6;
}
