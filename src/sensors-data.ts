export class SensorsData
{
	constructor(
		public readonly data: Buffer,
		public readonly unknownData: string,
		public readonly elapsed: number,
		public readonly speed: number,
		public readonly distance: number,
		public readonly incline: number,
		public readonly calories: number,
		public readonly pulse: number,
	) {}

	public equals(other: SensorsData)
	{
		return this.data.equals(other.data);
	}
}
