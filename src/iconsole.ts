import {Peripheral} from '@abandonware/noble';
import * as bluetooth from '@abandonware/noble';

import {Device, DeviceOptions} from './device';
import {DeviceConfigurationInterface} from './device-configurations';


export declare interface IConsoleOptions
{
	discoverMaxDevices?: number,
	deviceConnectionTimeout?: number,
	deviceConnectionRetries?: number,
	deviceRetryTimeout?: number,
	deviceWatchSensorsInterval?: number,
	deviceResponseTimeout?: number,
}

export class IConsole
{
	private readonly discoverMaxDevices: number|undefined;

	private readonly deviceConnectionTimeout: number|undefined;

	private readonly deviceConnectionRetries: number|undefined;

	private readonly deviceRetryTimeout: number|undefined;

	private readonly deviceWatchSensorsInterval: number|undefined;

	private readonly deviceResponseTimeout: number|undefined;

	constructor(
		private readonly deviceConfiguration: DeviceConfigurationInterface,
		options: IConsoleOptions = {},
	) {
		this.discoverMaxDevices = options.discoverMaxDevices;
		this.deviceConnectionTimeout = options.deviceConnectionTimeout;
		this.deviceConnectionRetries = options.deviceConnectionRetries;
		this.deviceRetryTimeout = options.deviceRetryTimeout;
		this.deviceWatchSensorsInterval = options.deviceWatchSensorsInterval;
		this.deviceResponseTimeout = options.deviceResponseTimeout;
	}

	public async findDevice(address: string): Promise<Device|null>
	{
		await this.waitForReady();
		return await this.discoverDevice(address);
	}

	private waitForReady(): Promise<void>
	{
		return new Promise(resolve => {
			if (bluetooth.state === 'poweredOn') {
				return resolve();
			}

			const handler = (state: string): void => {
				if (state === 'poweredOn') {
					bluetooth.removeListener('stateChange', handler);
					resolve();
				}
			};

			bluetooth.on('stateChange', handler);
		});
	}

	private async discoverDevice(address: string): Promise<Device|null>
	{
		await bluetooth.startScanningAsync(undefined, false);

		const deviceOptions: DeviceOptions = {
			connectionTimeout: this.deviceConnectionTimeout,
			connectionRetries: this.deviceConnectionRetries,
			retryTimeout: this.deviceRetryTimeout,
			watchSensorsInterval: this.deviceWatchSensorsInterval,
			responseTimeout: this.deviceResponseTimeout,
		};

		return new Promise<Device|null>(resolve => {
			let count = 0;

			const handler = (peripheral: Peripheral): void => {
				count++;

				if (typeof this.discoverMaxDevices !== 'undefined' && count >= this.discoverMaxDevices) {
					bluetooth.removeListener('discover', handler);
					return resolve(null);
				}

				if (peripheral.connectable && peripheral.address === address) {
					bluetooth.removeListener('discover', handler);
					return resolve(new Device(this.deviceConfiguration, peripheral, deviceOptions));
				}
			};

			bluetooth.on('discover', handler);
		}).then(async device => {
			await bluetooth.stopScanningAsync();
			return device;
		})
	}
}
