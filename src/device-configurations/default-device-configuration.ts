import {DeviceConfigurationInterface} from './device-configuration-interface';
import {SensorsData} from '../sensors-data';
import {kilometersPerHourToMetersPerSecond} from "../utils";


const DATA_PREFIX = Buffer.from('f0b2', 'hex');

/**
 * Works with
 * 		- Housefit Spiro 40 iRun
 *
 * f0 b2 01 d3 1f 37 02 48 02 32 01 01 01 20 01 03 04 03 78
 *
 * [0-3] - ?
 * [4] - seconds
 * [5] - minutes
 * [6] - distance in kilometers + 1
 * [7] - distance in meters + 1
 * [8] - hundreds of calories + 1
 * [9] - calories + 1
 * [10] - ?
 * [11] - pulse
 * [12] - ?
 * [13] - speed in km/h * 10
 * [14] - incline in %
 * [15-18] - ?
 */
export class DefaultDeviceConfiguration implements DeviceConfigurationInterface
{
	public readonly serviceConsoleUuid: string = '49535343fe7d4ae58fa99fafd205e455';

	public readonly consoleWriteCharacteristicUuid: string = '49535343884143f4a8d4ecbe34729bb3';

	public readonly consoleNotifyCharacteristicUuid: string = '495353431e4d4bd9ba6123c647249616';

	public readonly pingRequestData: Buffer = Buffer.from('f0a0010192', 'hex');

	public readonly pingResponseData: Buffer = Buffer.from('f0b001d374', 'hex');

	public readonly sensorsStateRequestData: Buffer = Buffer.from('f0a201d366', 'hex');

	public parseSensorsData(data: Buffer): SensorsData
	{
		if (!data.slice(0, 2).equals(DATA_PREFIX)) {
			throw new Error('Invalid data');
		}

		const elapsed = (data.readUInt8(5) - 1) * 60 + data.readUInt8(4) - 1;
		const speed = kilometersPerHourToMetersPerSecond((data.readUInt8(13) - 1) / 10);
		const distance = ((data.readUInt8(6)) - 1) * 1000 + (data.readUInt8(7) - 1) * 10;
		const incline = data.readUInt8(14) - 1;
		const calories = (data.readUInt8(8) - 1) * 100 + data.readUInt8(9) - 1;
		const pulse = data.readUInt8(11) - 1;

		const unknownData: Array<string> = [
			data[0].toString(16),
			data[1].toString(16),
			data[2].toString(16),
			data[3].toString(16),
			'__',
			'__',
			'__',
			'__',
			'__',
			'__',
			data[10].toString(16),
			'__',
			data[12].toString(16),
			'__',
			'__',
			data[15].toString(16),
			data[16].toString(16),
			data[17].toString(16),
			data[18].toString(16),
		];

		return new SensorsData(
			data,
			unknownData.map(b => b.padStart(2, '0')).join(' '),
			elapsed,
			speed,
			distance,
			incline,
			calories,
			pulse,
		);
	}
}
