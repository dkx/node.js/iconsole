import {SensorsData} from '../sensors-data';


export interface DeviceConfigurationInterface
{
	readonly serviceConsoleUuid: string;

	readonly consoleWriteCharacteristicUuid: string;

	readonly consoleNotifyCharacteristicUuid: string;

	readonly pingRequestData: Buffer;

	readonly pingResponseData: Buffer;

	readonly sensorsStateRequestData: Buffer;

	parseSensorsData(data: Buffer): SensorsData;
}
